package com.yusuf.app.agrosindonesiatest.util

import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.yusuf.app.agrosindonesiatest.R
import kotlin.math.roundToInt

@BindingAdapter("app:textStyle")
fun setTextStyle(view: TextView, type: String) {
    when (type) {
        "bold" -> view.setTypeface(null, Typeface.BOLD)
        "italic" -> view.setTypeface(null, Typeface.ITALIC)
        else -> view.setTypeface(null, Typeface.NORMAL)
    }
}

@BindingAdapter("app:tint")
fun setTint(view: ImageView, tint: Int) {
    view.setColorFilter(tint)
}

@BindingAdapter("android:layout_marginTop")
fun setTopMargin(view: View, topMargin: Float) {
    val layoutParams = view.layoutParams as MarginLayoutParams
    layoutParams.setMargins(
        layoutParams.leftMargin, topMargin.roundToInt(),
        layoutParams.rightMargin, layoutParams.bottomMargin
    )
    view.layoutParams = layoutParams
}


@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: Any?) {
//    Timber.e("load image: $url")
//    view.doOnLayout {
//        Glide.with(view.context).load(thumbnail(url, view.measuredWidth))
    Glide.with(view.context).load(url)
        .thumbnail(0.1f)
        .placeholder(R.drawable.ic_default)

//            .error(Glide.with(view.context).load(url).thumbnail(0.1f))
//            .placeholder(R.color.ltgray)
        .into(view)
//    }
}
