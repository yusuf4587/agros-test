package com.yusuf.app.agrosindonesiatest.ui.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yusuf.app.agrosindonesiatest.db.UserDataStore
import com.yusuf.app.agrosindonesiatest.model.LoginResponse
import com.yusuf.app.agrosindonesiatest.util.PreferenceClass
import com.yusuf.app.agrosindonesiatest.util.Resource
import kotlinx.coroutines.launch

class LoginViewModel @ViewModelInject constructor(
    private val repository: LoginRepository,
    private val store: UserDataStore
) : ViewModel() {
    val email by lazy { MutableLiveData<String>() }
    val password by lazy { MutableLiveData<String>() }
    val allowLogin by lazy { MutableLiveData<Boolean>(false) }
    val loginResponse: MutableLiveData<Resource<LoginResponse>> = MutableLiveData()


    fun fetchLogin() = viewModelScope.launch {
        loginResponse.value = Resource.Loading()
        try {
            val response = repository.fetchlogin(mapOf("email" to email.value, "password" to password.value))
            if (response.isSuccessful) {
                loginResponse.value = Resource.Success(response.body())
                storeLogin(response.body()!!.data)
            } else {
                loginResponse.value = Resource.Failed(response.message()) // return null karena bug di api , jika user pass salah respon seharusnya bukan 404
            }
        } catch (e: Exception) {
            loginResponse.value = Resource.Exception()
        }
    }

    private fun storeLogin( data: LoginResponse.LoginData ) = viewModelScope.launch {
        store.login(
            data
        )
    }


}