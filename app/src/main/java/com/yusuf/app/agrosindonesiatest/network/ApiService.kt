package com.yusuf.app.agrosindonesiatest.network

import com.yusuf.app.agrosindonesiatest.model.DeliveryResponse
import com.yusuf.app.agrosindonesiatest.model.LoginResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @POST("login")
    suspend fun login(
        @Body data: Any
    ): Response<LoginResponse>

    @GET("delivery")
    suspend fun delveryList(
        @Query("type") type:String,
        @Query("limit") limit:String
    ): Response<DeliveryResponse>
}