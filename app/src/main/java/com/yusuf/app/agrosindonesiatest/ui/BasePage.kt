package com.yusuf.app.agrosindonesiatest.ui

import android.app.ProgressDialog
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.yusuf.app.agrosindonesiatest.R
import timber.log.Timber

open class BasePage : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        Timber.e("build version sdk ${Build.VERSION.SDK_INT}")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val view = window.decorView
            var flags = view.systemUiVisibility
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//            flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            view.systemUiVisibility = flags
        } else {
            window.statusBarColor = ContextCompat.getColor(this, R.color.teal_200)
        }

    }

    fun dismissloading() {
        loadingDialog?.dismiss()
    }

    var loadingDialog: ProgressDialog? = null
    fun loading(
        title: String = "Mohon tunggu",
        msg: String = "Sedang memproses ...",
        indeterminate: Boolean = true
    ) {
        dismissloading()
        loadingDialog = ProgressDialog.show(this, title, msg, indeterminate)
    }

    fun toast(msg: String) {
        Timber.e("show toast: $msg\nfrom: ${javaClass.simpleName}")
        if (msg.isNotEmpty())
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

}