package com.yusuf.app.agrosindonesiatest.ui.splash

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.yusuf.app.agrosindonesiatest.R
import com.yusuf.app.agrosindonesiatest.ui.dashboard.DashboardPage
import com.yusuf.app.agrosindonesiatest.ui.login.LoginPage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import android.content.pm.PackageInfo

import android.content.pm.PackageManager
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_splash_screen.*


@AndroidEntryPoint
class SplashScreen : AppCompatActivity() {
    private val viewModel: SplashViewModel by viewModels()
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        setupObserver()
        setupUI()
    }

    private fun setupUI() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        Timber.e("build version sdk ${Build.VERSION.SDK_INT}")
        val version = packageManager.getPackageInfo(this.packageName,0).versionName
        Timber.e("build version sdk ${version}")
        txt_version.setText("Ver ($version) Build For Test")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val view = window.decorView
            var flags = view.systemUiVisibility
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            view.systemUiVisibility = flags
        } else {
            window.statusBarColor = ContextCompat.getColor(this, R.color.teal_200)
        }

    }

    private fun setupObserver() {
        viewModel.loginPreferences.observe(this, Observer {
            val intent: Intent = when (it.api_token.isNotEmpty()) {
                true -> Intent(this, DashboardPage::class.java)
                else -> Intent(this, LoginPage::class.java)
            }
            intent.let {
                coroutineScope.launch {
                    delay(2000)
                    startActivity( it )
                    finish()
                }
            }
        })
    }
}