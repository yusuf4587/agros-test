package com.yusuf.app.agrosindonesiatest.ui.login

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.yusuf.app.agrosindonesiatest.databinding.LoginPageBinding
import com.yusuf.app.agrosindonesiatest.ui.BasePage
import com.yusuf.app.agrosindonesiatest.ui.dashboard.DashboardPage
import com.yusuf.app.agrosindonesiatest.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.login_page.*

@AndroidEntryPoint
class LoginPage : BasePage() {

    lateinit var binding: LoginPageBinding
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.viewholder = viewModel

        initAction()
        initObserver()
        setupObserver()

    }

    private fun initObserver() {
        viewModel.email.observe(this, checkAllowLogin)
        viewModel.password.observe(this, checkAllowLogin)
    }

    private fun initAction() {
        btn_login.setOnClickListener {
            viewModel.allowLogin.observe(this, Observer {
                if (it) {
                    viewModel.fetchLogin()
                } else {
                    toast("Form tidak Boleh Kosong")
                }
            })
            viewModel.allowLogin.removeObservers(this)

        }
    }

    private fun setupObserver() {
        viewModel.loginResponse.observe(this, Observer { resource ->
            when (resource) {
                is Resource.Loading -> {
                    loading()
                }
                is Resource.Success -> {
                    dismissloading()
                    toast("Login berhasil!")
                    Handler().postDelayed({
                        startActivity(Intent(this, DashboardPage::class.java))
                        finish()
                    }, 800)
                }
                is Resource.Failed -> {
                    toast("No ID atau password salah")
                    dismissloading()
                }
                is Resource.Exception -> {
                    dismissloading()
                }
            }
        })
    }


    private val checkAllowLogin by lazy {
        Observer<Any> {
            viewModel.allowLogin.postValue(
                viewModel.email.value?.isNotEmpty() == true &&
                        viewModel.password.value?.isNotEmpty() == true
            )
        }
    }
}