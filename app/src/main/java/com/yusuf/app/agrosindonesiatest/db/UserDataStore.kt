package com.yusuf.app.agrosindonesiatest.db

import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.preferences.Preferences
import androidx.datastore.preferences.createDataStore
import androidx.datastore.preferences.edit
import androidx.datastore.preferences.preferencesKey
import com.yusuf.app.agrosindonesiatest.model.LoginResponse
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject


private const val useId = "user_id"
private const val userApiToken = "user_api_token"
private const val userNik = "user_nik"
private const val userName = "user_name"
private const val userEmail = "user_email"
private const val userPhoto = "user_photo"
private const val userTelepon = "user_telepon"
private const val userAddress = "user_address"
private const val userRole = "user_role"
private const val userPoliceNumber = "user_police_number"
private const val userTruck = "user_truck"
private const val userBalance = "user_balance"
private const val userBankName = "user_bank_name"
private const val userBankCode = "user_bank_code"
private const val userAccountNumber = "user_account_number"

class UserDataStore @Inject constructor(
    @ActivityContext context: Context
) {
    private val dataStore: DataStore<Preferences> =
        context.createDataStore(name = "Agros.pref")

    companion object {
        var apiToken: String = ""
    }

    val loginPrefenceFlow: Flow<LoginResponse.LoginData> = dataStore.data
        .map { pref ->
            val userId = pref[preferencesKey<Int>(useId)] ?: 0
            apiToken = pref[preferencesKey<String>(userApiToken)] ?: ""
            val nik = pref[preferencesKey<String>(userNik)] ?: ""
            val name = pref[preferencesKey<String>(userName)] ?: ""
            val email = pref[preferencesKey<String>(userEmail)] ?: ""
            val photo = pref[preferencesKey<String>(userPhoto)] ?: ""
            val telepon = pref[preferencesKey<String>(userTelepon)] ?: ""
            val address = pref[preferencesKey<String>(userAddress)] ?: ""
            val role = pref[preferencesKey<String>(userRole)] ?: ""
            val policeNumber = pref[preferencesKey<String>(userPoliceNumber)] ?: ""
            val truck = pref[preferencesKey<String>(userTruck)] ?: ""
            val ballance = pref[preferencesKey<String>(userBalance)] ?: ""
            val bankName = pref[preferencesKey<String>(userBankName)] ?: ""
            val bankCode = pref[preferencesKey<String>(userBankCode)] ?: ""
            val accountNumber = pref[preferencesKey<String>(userAccountNumber)] ?: ""
            LoginResponse.LoginData(
                userId,
                apiToken,
                nik,
                name,
                email,
                photo,
                telepon,
                address,
                role,
                policeNumber,
                truck,
                ballance,
                bankName,
                bankCode,
                accountNumber,
            )
        }

    suspend fun login(login: LoginResponse.LoginData) {
        dataStore.edit { pref ->
            pref[preferencesKey<Int>(useId)] = login.id
            pref[preferencesKey<String>(userApiToken)] = login.api_token
            pref[preferencesKey<String>(userNik)] = login.nik
            pref[preferencesKey<String>(userName)] = login.name
            pref[preferencesKey<String>(userEmail)] = login.email
            pref[preferencesKey<String>(userPhoto)] = login.photo
            pref[preferencesKey<String>(userTelepon)] = login.telepon
            pref[preferencesKey<String>(userAddress)] = login.address
            pref[preferencesKey<String>(userRole)] = login.role
            pref[preferencesKey<String>(userPoliceNumber)] = login.nomor_polisi
            pref[preferencesKey<String>(userTruck)] = login.truck
            pref[preferencesKey<String>(userBalance)] = login.balance
            pref[preferencesKey<String>(userBankName)] = login.bank_name
            pref[preferencesKey<String>(userBankCode)] = login.bank_code
            pref[preferencesKey<String>(userAccountNumber)] = login.account_number
        }
    }
}