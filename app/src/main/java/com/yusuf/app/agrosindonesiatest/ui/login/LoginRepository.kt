package com.yusuf.app.agrosindonesiatest.ui.login

import com.yusuf.app.agrosindonesiatest.model.LoginResponse
import com.yusuf.app.agrosindonesiatest.network.ApiService
import retrofit2.Response
import javax.inject.Inject

class LoginRepository @Inject constructor(
    private val api: ApiService,
) {
    suspend fun fetchlogin(Body: Any): Response<LoginResponse> {
        return api.login(Body)
    }

}