package com.yusuf.app.agrosindonesiatest.ui.dashboard

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yusuf.app.agrosindonesiatest.databinding.DashboardHistoryItemBinding
import com.yusuf.app.agrosindonesiatest.model.DeliveryResponse.DeliveryData

class DeliveryAdapter() : RecyclerView.Adapter<ProductViewHolder>() {

    interface ProductItemListener {
        fun onClickedProduct(product: DeliveryData, qty: Int)
    }

    private val items = ArrayList<DeliveryData>()

    fun setItems(items: ArrayList<DeliveryData>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val binding: DashboardHistoryItemBinding =
            DashboardHistoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) =
        holder.bind(items[position])
}

class ProductViewHolder(private val itemBinding: DashboardHistoryItemBinding) :
    RecyclerView.ViewHolder(itemBinding.root) {
    private lateinit var Product: DeliveryData

    @SuppressLint("SetTextI18n")
    fun bind(item: DeliveryData) {
        this.Product = item
        itemBinding.data = item

    }


}

