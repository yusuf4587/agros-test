package com.yusuf.app.agrosindonesiatest.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yusuf.app.agrosindonesiatest.model.DeliveryResponse.DeliveryData
import com.yusuf.app.agrosindonesiatest.model.LoginResponse.LoginData


@Database(
    entities = [
        DeliveryData::class,
    ],
    exportSchema = false,
    version = 3
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun deliveryDao(): DeliveryDao
}