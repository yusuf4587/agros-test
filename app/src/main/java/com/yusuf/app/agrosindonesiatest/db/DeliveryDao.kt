package com.yusuf.app.agrosindonesiatest.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yusuf.app.agrosindonesiatest.model.DeliveryResponse.DeliveryData
import kotlinx.coroutines.flow.Flow

@Dao
interface DeliveryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: DeliveryData)

    @Query("SELECT * FROM DELIVERY ORDER BY ID ASC")
    fun getAll(): Flow<List<DeliveryData>>

}