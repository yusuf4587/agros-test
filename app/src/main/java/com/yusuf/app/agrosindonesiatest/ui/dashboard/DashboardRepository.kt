package com.yusuf.app.agrosindonesiatest.ui.dashboard

import com.yusuf.app.agrosindonesiatest.db.DeliveryDao
import com.yusuf.app.agrosindonesiatest.model.DeliveryResponse
import com.yusuf.app.agrosindonesiatest.model.DeliveryResponse.DeliveryData
import com.yusuf.app.agrosindonesiatest.network.ApiService
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject

class DashboardRepository @Inject constructor(
    private val apiService: ApiService,
    private val dao: DeliveryDao,
) {
    suspend fun fetchDelivery(
        type: String = "History",
        limit: String = "10"
    ): Response<DeliveryResponse> {
        return apiService.delveryList(type, limit)
    }

    suspend fun insertDelivery(deliveryData: DeliveryData) {
        return dao.insert(deliveryData)
    }

    fun getDelivery(): Flow<List<DeliveryData>> {
        return dao.getAll()
    }


}