package com.yusuf.app.agrosindonesiatest.ui.dashboard

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yusuf.app.agrosindonesiatest.db.UserDataStore
import com.yusuf.app.agrosindonesiatest.model.DeliveryResponse
import com.yusuf.app.agrosindonesiatest.model.LoginResponse
import com.yusuf.app.agrosindonesiatest.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber

class DashboardViewModel @ViewModelInject constructor(
    private val repository: DashboardRepository,
    private val store: UserDataStore
) : ViewModel() {

    val loginPreferences: MutableLiveData<LoginResponse.LoginData> = MutableLiveData()

    init {
        loginPreferencesFlow()
    }

    private fun loginPreferencesFlow() = viewModelScope.launch {
        store.loginPrefenceFlow.collect {
            loginPreferences.value = it
        }
    }
    fun loadDeliveryList():Flow<List<DeliveryResponse.DeliveryData>>{
        fetchDelivery()
        return repository.getDelivery()
    }



    val deliveryResponse: MutableLiveData<Resource<DeliveryResponse>> = MutableLiveData()
    fun fetchDelivery() = viewModelScope.launch {
        deliveryResponse.value = Resource.Loading()
        try {
            val response = repository.fetchDelivery()
            if (response.isSuccessful) {
                Timber.e("sukses ambil delivery ${response.body()!!.data}")
                val data = response.body()!!.data
                data.forEach {
                    repository.insertDelivery(it)
                }
                deliveryResponse.value = Resource.Success(response.body())
            } else {
                deliveryResponse.value = Resource.Failed(response.message())
            }
        } catch (e: Exception) {
            Timber.e("Exception $e")
            deliveryResponse.value = Resource.Exception()
        }
    }

}