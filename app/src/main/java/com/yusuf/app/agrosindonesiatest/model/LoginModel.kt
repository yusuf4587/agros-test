package com.yusuf.app.agrosindonesiatest.model

import androidx.room.Entity
import androidx.room.PrimaryKey


data class LoginResponse(
    val success:Boolean,
    val data:LoginData,
    val message:String,
){
    data class LoginData(
        val id:Int,
        val api_token:String,
        val nik:String,
        val name:String,
        val email:String,
        val photo:String,
        val telepon:String,
        val address:String,
        val role:String,
        val nomor_polisi:String,
        val truck:String,
        val balance:String,
        val bank_name:String,
        val bank_code:String,
        val account_number:String
    )
}
