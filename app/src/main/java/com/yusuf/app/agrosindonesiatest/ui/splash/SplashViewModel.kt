package com.yusuf.app.agrosindonesiatest.ui.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yusuf.app.agrosindonesiatest.db.UserDataStore
import com.yusuf.app.agrosindonesiatest.model.LoginResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SplashViewModel @ViewModelInject constructor(
    private val store: UserDataStore
) : ViewModel() {
    val loginPreferences: MutableLiveData<LoginResponse.LoginData> = MutableLiveData()
    init {
        loginPreferencesFlow()
    }
    private fun loginPreferencesFlow() = viewModelScope.launch {
        store.loginPrefenceFlow.collect {
            loginPreferences.value = it
        }
    }
}

