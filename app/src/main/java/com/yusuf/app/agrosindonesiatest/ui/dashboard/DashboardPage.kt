package com.yusuf.app.agrosindonesiatest.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.OrientationHelper
import com.yusuf.app.agrosindonesiatest.R
import com.yusuf.app.agrosindonesiatest.databinding.DashboardPageBinding
import com.yusuf.app.agrosindonesiatest.ui.BasePage
import com.yusuf.app.agrosindonesiatest.ui.splash.SplashViewModel
import com.yusuf.app.agrosindonesiatest.util.LinearSpaceDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import androidx.recyclerview.widget.LinearSnapHelper

import androidx.recyclerview.widget.SnapHelper
import com.yusuf.app.agrosindonesiatest.util.Resource


@AndroidEntryPoint
class DashboardPage : BasePage() {

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dashboard_top_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_user) {
            Timber.e("menu user click")
        } else {
            Timber.e("menu callendar click")
        }
        return super.onOptionsItemSelected(item)
    }

    lateinit var binding: DashboardPageBinding
    private val viewModel: DashboardViewModel by viewModels()
    private lateinit var deliveryAdapter: DeliveryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DashboardPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        setupObserver()
        setupRecyclerview()
    }

    private fun setupRecyclerview() {
        deliveryAdapter = DeliveryAdapter()
        binding.rvDelivery.apply {
            addItemDecoration(
                LinearSpaceDecoration(
                    OrientationHelper.HORIZONTAL,
                    resources.getDimensionPixelSize(R.dimen._10sdp),
                    includeTop = true,
                    includeBottom = true,
                    resources.getDimensionPixelSize(R.dimen._16sdp)
                )
            )
            adapter = deliveryAdapter
        }
        val helper: SnapHelper = LinearSnapHelper()
        helper.attachToRecyclerView(binding.rvDelivery)
    }


    private fun setupObserver() {
        viewModel.loginPreferences.observe(this, Observer {
            binding.data = it
            Timber.e("load loginPreferences $it")

            lifecycleScope.launch {
                viewModel.loadDeliveryList().collectLatest {
                    try {
                        val data = it
                        if (!data.isNullOrEmpty()) {
                            deliveryAdapter.setItems(ArrayList(data))
                        } else {
                            viewModel.fetchDelivery()
                        }
                    } catch (e: Exception) {
                        Timber.e("load Delivery $e")
                    }
                }
            }
        })

        viewModel.deliveryResponse.observe(this, Observer { resource ->
            when (resource) {
                is Resource.Loading -> {
                    loading()
                }
                is Resource.Success -> {
                    dismissloading()
                }
                is Resource.Failed -> {
                    toast("No ID atau password salah")
                    dismissloading()
                }
                is Resource.Exception -> {
                    dismissloading()
                }
            }
        })


    }
}