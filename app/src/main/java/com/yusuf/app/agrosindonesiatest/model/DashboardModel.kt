package com.yusuf.app.agrosindonesiatest.model

import androidx.room.Entity
import androidx.room.PrimaryKey


data class DeliveryResponse(
    val succes:String,
    val data:List<DeliveryData>,
    val message:String,
    val ballance:String,
){
    @Entity(tableName = "delivery")
    data class DeliveryData(
        @PrimaryKey val id:Int,
        val client:String,
        val nama_pelanggan:String,
        val tujuan:String,
        val reference:String,
        val do_number:String,
        val po_number:String,
        val waktu_muat:String,
        val alamat_muat:String,
        val kota_muat:String,
        val waktu_bongkar:String,
        val alamat_bongkar:String,
        val kota_bongkar:String,
        val nama_barang:String,
        val volume:Int,
        val satuan:String,
        val sangu:Int,
        val jarak:Int,
        val status_ambil:Boolean,
        val status_pengiriman:String,
        val driver:String,
        val nomor_polisi :String,
        val type:String,
        val status:String
    )
}
