package com.yusuf.app.agrosindonesiatest.di

import com.yusuf.app.agrosindonesiatest.db.DeliveryDao
import com.yusuf.app.agrosindonesiatest.network.ApiService
import com.yusuf.app.agrosindonesiatest.ui.dashboard.DashboardRepository
import com.yusuf.app.agrosindonesiatest.ui.login.LoginRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
object RepositoryModule {

    @Provides
    fun provideLoginRepository(apiService: ApiService): LoginRepository {
        return LoginRepository(apiService)
    }

    @Provides
    fun provideDashBoardRepository(apiService: ApiService,deliveryDao: DeliveryDao): DashboardRepository {
        return DashboardRepository(apiService,deliveryDao)
    }

}