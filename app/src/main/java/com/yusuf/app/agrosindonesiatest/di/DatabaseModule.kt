package com.yusuf.app.agrosindonesiatest.di

import android.content.Context
import androidx.room.Room
import com.yusuf.app.agrosindonesiatest.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun providesDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        "agros.db"
    ).fallbackToDestructiveMigration().build()


    @Singleton
    @Provides
    fun providesDeliveryDao(db: AppDatabase) = db.deliveryDao()


}